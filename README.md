# NoCowboys knowledge and skill test #

Hi! Thanks for taking part in this knowledge and skill test. NoCowboys is looking for a new developer who can come in 
and take over all aspects of development for their website, so it's important you know your stuff! This is a simple test
to see what you do and don't know. It'll cover basic PHP stuff, MVC, unit tests, migrations, and front-end stuff. Give
it a go and do your best.

NoCowboys has a lead PHP developer with 10 years of London experience on the board, who will oversee development, 
architecture and PRs, so we're looking for very high standards and best practices. We're looking for proper commenting,
code styles and attention to detail, logical and clean design using OO and MVC etc. Any decision you make about how
you code and what you do should have a reason :) We'll also be looking at naming conventions (what does Zend recommend, 
and why?)

If you're unfamiliar with Zend, or any other bits of the system, part of this test will be how to research the _best_ 
(not just the first) methods for completing a task. Short-cuts aren't good; we want to see how you write sustainable
and maintainable code. We would rather you take longer to do something _right_, instead of rushing it through.

## This test##

In the hope of making this at least a bit interesting, let's create a simple app that allows a bar to count the volume
of beer in bottles or a case of beer. We'll create a table in the DB that represents a beer, which will let us calculate 
the number/volume of beer left! It's important to restock beer when it's getting low. Yay! Beer!

### Tables/architecture

Let's start with a couple of simple ones. We'll create them using migrations, so no need to create them in the DB now.
It's up to you to name these tables and fields the way you think is best, along with defaults, required fields etc.

* A beer manufacturer table that will contain:
    * An ID (obviously)
    * Name
    * Website

* A beer table that will contain:
    * An ID (obviously)
    * Name of the beer
    * Manufacturer ID
    * Millilitres of beer per bottle
    * Number of bottles per case
    * Number of bottles currently in stock
    * Cost per bottle
    * Last purchase date

## Installation ##

* Fork this repo
* Clone your repo out to your local machine
* Install composer and then `composer install` to install the basics of what we need
* Install `npm` and run `npm install` and `npm install grunt-cli -g`. This will install Grunt which we use to compile our SASS
   and also do things like minify JS etc.

## Tasks ##

1. Create a database in MySQL
2. Set up a config file that points Zend to your new database
3. We use [Phinx](https://phinx.org/) to create new migrations, or changes to a database. This is important because it keeps track of database
changes that can be run across all development and production systems.
    1. Create a new migration to add the manufacturer table
    2. Create a new migration to add the beer table
    3. Run these migrations; you should now see them in your DB
    4. To save you a bit of time, manually add a manufacturer into the DB table. We'll only create controler actions for adding beers
4. Create models for these tables in Zend
5. Create a controller, some actions, views and forms for "beers" that allows us to view the list of beers,
add a new beer, and edit an existing beer (mmmmm, beer)
    1. Zend has nice support for creating forms and form elements (including labels), so research and use these. Make sure you 
     also use suitable validators for the form elements. Create a dropdown for the manufacturers
    2. In the actions, if the new/edited beer is valid, save/create the beer and redirect the 
     user back to the list of beers
6. Let's write a function that allows us to calculate how much volume is available for a given type of beer. Think about where this function is 
 best placed in the system. We know the total number of bottles left, as well as the volume per bottle, so we can work this out simply
    1. Write a function to calculate the volume of beer (in `ml`) remaining
    2. Write a function that returns how much volume of beer is remaining in _litres_. How can you reuse the first function for this?
7. Unit tests for these types of functions (calculations) are important, and valuable. Write a unit test (using PHPUnit, already 
 installed using Composer above) to ensure both functions return the expected values
    1. To run your tests, in your command line run `vendor/bin/phpunit`
8. In the repo, you'll find a directory `test-designs` that contains a design for a form. Your challenge is to style your form and the 
 header/footer to match this design as closely as possible. We're very particular about our design (spacing, colours, layout), so make
 sure it looks very similar!
    1. In the directory `public/styles/scss` you'll find the beginnings of a group of SASS files. In here you'll add the correct files and
     styles for this
    2. In the `layout.phtml` file, you can add the header and footer HTML (this layout file is used for all pages across the project)
    3. We've already installed Grunt above, so on your command line you can run `grunt` which will compile the SASS, but you can also
     run `grunt watch` which will "watch" any SCSS files for changes and automatically compile them which is a lot faster than doing it
     manually
     
 Once you're happy with all of this, commit your changes and send me the repo details! Well done!
 
 Any questions or issues, let me know.