module.exports = function (grunt) {

	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.initConfig({
		sass: {
			options: {
				sourceMap: true,
				sourceMapEmbed: true
			},
			production: {
				files: {
					"nocowboys/public/styles/nocowboys.css": "nocowboys/public/styles/scss/main.scss"
				}
			}
		},
		watch: {
			css: {
				files: ["nocowboys/public/styles/scss/**/*.scss"],
				tasks: ["sass"]
			}
		},
	});

	grunt.registerTask("default", ['sass']);
};