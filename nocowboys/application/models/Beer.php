<?php

class Application_Model_Beer extends Zend_Db_Table_Abstract
{
	protected $_name = 'beer';
	protected $_primary = 'id';
	//protected $_dependentTables = array('Application_Model_Manufacturer');

/*
	protected $_referenceMap    = array(
        'Manufacturer' => array(
            'columns'           => 'manufacturer_id',
            'refTableClass'     => 'Application_Model_Manufacturer',
            'refColumns'        => 'id'
        )
    );
    */

    public function fetchAll($where = null, $order = null, $count = null, $offset = null)
    {
        $entries = array ();
        if (null !== ($resultSet = parent::fetchAll($where, $order, $count, $offset))) 
        {
            foreach ($resultSet as $row) 
            {
                $manufacturer = $row->findDependentRowset(
                    'Application_Model_Manufacturer'
                );

                $manufacturer = $manufacturer->current();
                $entries[] = array(
                    'beer' => $row,
                    'manufacturer' => $manufacturer,
                    'totalMl' => $this->getTotalVolumeMl($row),
                    'totalL'  => $this->getTotalVolumeL($row)
                );
            }

            return $entries;
        }
    }

    public function getTotalVolumeMl($beerRow){

        $ml_per_bottle = $beerRow->ml_per_bottle;
        $bottles_in_stock = $beerRow->bottles_in_stock;

        return $ml_per_bottle*$bottles_in_stock;
    }

    public function getTotalVolumeL($beerRow){
        $volumeInMl = $this->getTotalVolumeMl($beerRow);

        return $volumeInMl !=0 ? $volumeInMl/1000 : 0;
    }

}
