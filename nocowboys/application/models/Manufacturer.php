<?php

class Application_Model_Manufacturer extends Zend_Db_Table_Abstract
{
	protected $_name = 'manufacturer';
	protected $_primary = 'id';
	protected $_dependentTables = array('Application_Model_Beer');

	protected $_referenceMap    = array(
        'Beer' => array(
            'columns'           => 'id',
            'refTableClass'     => 'Application_Model_Beer',
            'refColumns'        => 'manufacturer_id'
        )
    );
}

