<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $beerModel = new Application_Model_Beer;
        $beerSet = $beerModel->fetchAll();
        
        $this->view->assign('beerSet', $beerSet);
    }

    public function addbeerAction()
    {
        $form = new Application_Form_Beer();

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            if ($form->isValid($postData)) {  
                $formData = $form->getValues();
                
                if($formData['last_purchase'] == ""){
                    $formData['last_purchase'] = null;
                }

                $beerModel = new Application_Model_Beer; 
                if($beerModel->insert($formData)){
                    $this->_redirect('/'); 
                }
            }  
            else $form->populate($postData); 
        }

        $this->view->form = $form;
    }

    public function editbeerAction()
    {
        $id = $this->getRequest()->getParam('id');
        $form = new Application_Form_Beer();

        if((int)$id > 0){
            if ($this->getRequest()->isPost()) {
                $postData = $this->getRequest()->getPost();  
                if ($form->isValid($postData)) {  
                    $formData = $form->getValues();  
                     
                    if($formData['last_purchase'] == ""){
                        $formData['last_purchase'] = null;
                    }

                    $beerModel = new Application_Model_Beer; 
                    if($beerModel->update($formData, 'id = '.$id)){
                        $this->_redirect('/'); 
                    }  
                }  
                else $form->populate($postData);  
            }  
            else {  
                $beerModel = new Application_Model_Beer;
                $beerRow = $beerModel->fetchRow('id='.(int)$id);  

                $form->populate($beerRow->toArray());

                $hidden = new Zend_Form_Element_Hidden('id');  
                $hidden->setValue($id);

                $form->addElement($hidden);  
            }

        }else{
            $this->_redirect('/'); 
        }
          

        $this->view->form = $form;
        
    }


}

