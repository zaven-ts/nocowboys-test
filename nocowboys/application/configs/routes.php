<?php

	$route = new Zend_Controller_Router_Route(
		    'addBeer',
		    array(
		        'controller' => 'index',
		        'action'     => 'addBeer'
		    ) 
	);

	$router->addRoute('addBeer', $route);


	$router->addRoute(
    'editBeer',
    new Zend_Controller_Router_Route('editBeer/:id',
                                     array('controller' => 'index',
                                           'action' => 'editBeer'))
	);