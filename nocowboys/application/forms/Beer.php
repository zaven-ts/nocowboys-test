<?php

class Application_Form_Beer extends Zend_Form
{

    public function init()
    {

        $this->setMethod('post');

        $this->addElement('text', 'name', array(
            'label'      => 'Beer name:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(3))
            ),
            'attribs'	 => array('class' => 'form-control')
        ));

        $this->addElement('select', 'manufacturer_id', array(
            'label'      => 'Manufacturer name:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                //array('validator' => 'StringLength', 'options' => array(3))
            ),
            'attribs'	 => array('class' => 'form-control'),
            'multiOptions' => $this->getManufacturerList(),
        ));

        $this->addElement('text', 'ml_per_bottle', array(
            'label'      => 'Millilitres of beer per bottle:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'regex', 'options' => array('/[0-9]/i'))
            ),
            'attribs'	 => array('class' => 'form-control')
        ));

        $this->addElement('text', 'bottles_per_case', array(
            'label'      => 'Number of bottles per case:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'regex', 'options' => array('/[0-9]/i'))
            ),
            'attribs'	 => array('class' => 'form-control')
        ));

        $this->addElement('text', 'bottles_in_stock', array(
            'label'      => 'Number of bottles currently in stock:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'regex', 'options' => array('/[0-9]/i'))
            ),
            'attribs'	 => array('class' => 'form-control')
        ));

        $this->addElement('text', 'cost_per_bottle', array(
            'label'      => 'Cost per bottle:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'regex', 'options' => array('/^-?(?:\d+|\d*\.\d+)$/'))
            ),
            'attribs'	 => array('class' => 'form-control')
        ));

        $this->addElement('text', 'last_purchase', array(
            'label'      => 'Last purchase date:',
            'required'   => false,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('date', false, array('yyyy-MM-dd'))
            ),
            'attribs'	 => array(
            	'class' => 'form-control',
            	'placeholder' => 'yyyy-MM-dd'
            	)
        ));


        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Save',
            'attribs'	 => array('class' => 'btn btn-success')
        ));

    }

    private function getManufacturerList(){
    	$manufModel = new Application_Model_Manufacturer;
        $manufacturerSet = $manufModel->fetchAll();

        $manufacturerList = array();
		foreach($manufacturerSet as $manufacturer){
			$manufacturerList[$manufacturer->id] = $manufacturer->name;
		}
		return $manufacturerList;
    }


}

