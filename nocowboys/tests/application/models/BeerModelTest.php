<?php

class BeerModelTest extends Zend_Test_PHPUnit_ControllerTestCase
{
	private $beerModel;

	public function setUp() {

        $this->bootstrap = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');
        parent::setUp();

        $this->beerModel = new Application_Model_Beer;
    
    }

    public function testTotalValueMl() {

        $beerRow = $this->beerModel->fetchRow();
        if(!$beerRow){
        	echo "Please, add a beer at first.";
        }else{
        	$result = $this->beerModel->getTotalVolumeMl($beerRow);

        	$expectingResult = $beerRow->ml_per_bottle * $beerRow->bottles_in_stock;

        	$this->assertEquals($result, $expectingResult);
        }
        
    }

    public function testTotalValueL() {

        $beerRow = $this->beerModel->fetchRow();
        if(!$beerRow){
        	echo "Please, add a beer at first.";
        }else{
        	$result = $this->beerModel->getTotalVolumeL($beerRow);

        	$expectingResult = ($beerRow->ml_per_bottle * $beerRow->bottles_in_stock) / 1000;

        	$this->assertEquals($result, $expectingResult);
        }
        
        
    }
}