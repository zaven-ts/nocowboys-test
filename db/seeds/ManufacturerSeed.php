<?php

use Phinx\Seed\AbstractSeed;

class ManufacturerSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = array(
          array(
              'name'    => 'Heineken',
              'website' => 'http://www.heineken.com',
          ),
          array(
              'name'    => 'Gyumri',
              'website' => 'http://gyumribeer.am/',
          )
        );

        $manufacturer = $this->table('manufacturer');
        $manufacturer->insert($data)
              ->save();
    }
}
