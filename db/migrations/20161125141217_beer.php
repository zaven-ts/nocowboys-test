<?php

use Phinx\Migration\AbstractMigration;

class Beer extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('beer');
        $table->addColumn('name', 'string', array('limit' => 40))
              ->addColumn('manufacturer_id', 'integer')
              ->addColumn('ml_per_bottle', 'float')
              ->addColumn('bottles_per_case', 'integer')
              ->addColumn('bottles_in_stock', 'integer')
              ->addColumn('cost_per_bottle', 'float')
              ->addColumn('last_purchase', 'datetime', array('null' => true))
              ->addForeignKey('manufacturer_id', 'manufacturer', 'id', array('delete'=> 'NO_ACTION', 'update'=> 'NO_ACTION'))
              ->create();
    }
}
